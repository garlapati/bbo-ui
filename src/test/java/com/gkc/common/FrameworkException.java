package com.gkc.common;

import org.junit.Assert;
import org.openqa.selenium.WebDriverException;

public class FrameworkException extends RuntimeException {

    public FrameworkException(String message, Exception exception){
        Screenshot.getBase64Screenshot();
        if(exception instanceof WebDriverException){
            if(exception.getMessage().contains("chrome not reachable")){
                DriverActions.getInstance().reset();
            } else {
                DriverActions.getInstance().quitBrowser();
            }
        } else {
            DriverActions.getInstance().quitBrowser();
        }
        Assert.fail("Message "+message + System.lineSeparator() + "Exception :"+exception.getMessage());
    }

    public FrameworkException(String message){
        Screenshot.getBase64Screenshot();
        DriverActions.getInstance().quitBrowser();
        Assert.fail("Message "+message+System.lineSeparator());
    }

    //Exception Handling for screen shot

    public FrameworkException(Exception exception){
        Assert.fail("Message: Failed to take the screen shot "+ System.lineSeparator()+"Exception : "+exception.getMessage());
    }
}
