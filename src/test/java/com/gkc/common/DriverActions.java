package com.gkc.common;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Properties;

public class DriverActions {

    private long timeOutinSeconds = 90;
    private long elementTimeout = 30;
    private static DriverActions driverActions;
    private static WebDriver driver;
    private Properties property = ARTProperties.loadProperties();

    public static DriverActions getInstance(){
        if(driverActions==null){
            driverActions = new DriverActions();
        }
        return driverActions;
    }

    public WebDriver getDriver(){
        if(driver == null){
            driver = WebDriverFactory.initializeDriver(WebDriverFactory.Browser.valueOf(property.get("browser").toString()));
        }
        return driver;
    }

    public void reset(){
        driver = null;
    }

    public void navigateTo(String url){
        try{
            driver.navigate().to(url);
        } catch (Exception ex) {
            throw new FrameworkException("Error in accessing the url ", ex);
        }
    }

    public void waitUntilPageReadyStateComplete(){
        try{
            ExpectedCondition<Boolean> pageLoadCondition = new
                    ExpectedCondition<Boolean>() {
                        @Override
                        public Boolean apply(WebDriver webDriver) {
                            return ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete");
                        }
                    };
            WebDriverWait wait = new WebDriverWait(driver, 60);
            wait.until(pageLoadCondition);
        } catch (TimeoutException ex) {
            throw new FrameworkException("Time Out Exception", ex);
        } catch (Exception ex){
            throw new FrameworkException("Error in loading the page completely", ex);
        }
    }

    public void switchToFrameByElement(List<WebElement> elements){
        try{
            if(elements != null) {
                driver.switchTo().defaultContent();
                for (WebElement element : elements) {
                    new WebDriverWait(driver, elementTimeout)
                            .until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
                }
            }
        } catch (Exception e) {
            throw new FrameworkException("Could not click the element", e);
        }
    }

    public void switchToFirstAvailableFrame(){
        try{
            List<WebElement> elements = new WebDriverWait(driver, elementTimeout)
                    .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.tagName("frame")));
            for (WebElement element : elements) {
                driver.switchTo().frame(element);
            }
        } catch (Exception e) {
            throw new FrameworkException("Could not find frame", e);
        }
    }

    public void clickButton(WebElement element){
        try{
            new WebDriverWait(driver, elementTimeout).until(ExpectedConditions.elementToBeClickable(element));
            element.click();
        } catch (Exception e) {
            throw new FrameworkException("Could not click the element", e);
        }
    }

    public void select(WebElement element, String value){
        try{
            new WebDriverWait(driver, elementTimeout).until(ExpectedConditions.elementToBeClickable(element));
            Select dropdown = new Select(element);
            dropdown.selectByVisibleText(value);
        } catch (Exception e) {
            throw new FrameworkException("Could not click the element", e);
        }
    }

    public void checkBox(WebElement element, Boolean check){
        try{
            new WebDriverWait(driver, elementTimeout).until(ExpectedConditions.elementToBeSelected(element));
            if(check != element.isSelected()){
                element.click();
            }
        } catch (Exception e) {
            throw new FrameworkException("Could not click the element", e);
        }
    }

    public void clickVisibleButton(WebElement element){
        try{
            new WebDriverWait(driver, elementTimeout).until(ExpectedConditions.visibilityOf(element));
            element.click();
        } catch (Exception e) {
            throw new FrameworkException("Could not click the element", e);
        }
    }

    public void clickButtonAction(WebElement element){
        try{
            new WebDriverWait(driver, elementTimeout).until(ExpectedConditions.visibilityOf(element));
            JavascriptExecutor executor = (JavascriptExecutor) DriverActions.driver;
            executor.executeScript("arguments[0].click();", element);
        } catch (Exception e) {
            throw new FrameworkException("Could not click the element", e);
        }
    }

    public String getElementText(WebElement element){
        try{
            new WebDriverWait(driver, elementTimeout).until(ExpectedConditions.visibilityOf(element));
            return element.getText();
        } catch (Exception e) {
            return null;
        }
    }

    public boolean waitUntilElementVisible(WebElement element){
        if((new WebDriverWait(driver, elementTimeout)).until(ExpectedConditions.visibilityOf(element)) != null)
            return true;
        else
            return false;
    }

    public Boolean isElementDisplayed(WebElement element){
        try{
            if((new WebDriverWait(driver, elementTimeout)).until(ExpectedConditions.visibilityOf(element)) != null){
                element.isDisplayed();
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException | TimeoutException e) {
            return false;
        }
    }

    public Boolean isCaptchaDisplayed(WebElement element){
        try{
            if((new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element)) != null){
                element.isDisplayed();
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException | TimeoutException e) {
            return false;
        }
    }

    public void waitUntilLoadingCompletes(){
        try{
            List<WebElement> webElements = driver.findElements(By.xpath("//div[@id=\"processingDiv\"]"));
            for (WebElement webElement : webElements) {
                new WebDriverWait(driver, timeOutinSeconds).until(ExpectedConditions.attributeContains(
                        driver.findElement(By.xpath("//div[@id=\"processingDiv\"]")), "display", "none"));
            }
        } catch (TimeoutException e) {
            throw new FrameworkException("Waiting for an element took more than expected time", e);
        } catch (Exception e){
            throw new FrameworkException("Could not find loading scree", e);
        }
    }

    public void enterText(WebElement element, String value){
        try{
            if(!value.equals("")){
                new WebDriverWait(driver, elementTimeout).until(
                        ExpectedConditions.visibilityOf(element));
                element.click();
                element.sendKeys(value);
                element.sendKeys(Keys.TAB);
            }
        } catch (Exception e) {
            throw new FrameworkException("Error during entering the value to text field", e);
        }
    }

    public void quitBrowser(){
        driver.close();
        driver.quit();
        driver = null;
    }

    public void scrollToElement(WebElement element){
        try{
            new WebDriverWait(driver, elementTimeout).until(
                    ExpectedConditions.visibilityOf(element));
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
        } catch (Exception e) {
            throw new FrameworkException("Could not scroll to the element", e);
        }
    }

    public void switchToDefaultFrame(){
        try{
            driver.switchTo().defaultContent();
            driver.switchTo().parentFrame();
        } catch (Exception e) {
            throw new FrameworkException("could not switch to dafault element",e);
        }
    }

    public void waitForUrlToBe(String urlToBe){
        try{
            if(!urlToBe.equals("")){
                new WebDriverWait(driver, elementTimeout).until(
                        ExpectedConditions.urlToBe(urlToBe));
            }
        } catch (Exception e) {
            throw new FrameworkException("Failed during waiting for URL to be", e);
        }
    }

    public String getText(WebElement userTitle){
        String text = null;
        try{
            if(Objects.nonNull(userTitle)){
                text = userTitle.getText();
            }
        } catch (Exception e) {
            throw new FrameworkException("Failed to identify the element", e);
        }
        return text;
    }

    public void clearText(WebElement element){
        try{
            new WebDriverWait(driver, elementTimeout).until(
                    ExpectedConditions.visibilityOf(element));
            element.click();
        } catch (Exception e) {
            throw new FrameworkException("Time out exception for the visibility of element", e);
        }
    }

    public List<WebElement> getAllRowsFromTable(WebElement table, List<WebElement> row){
        try{
            new WebDriverWait(driver, elementTimeout).until(
                    ExpectedConditions.visibilityOf(table));
            return row;
        } catch (Exception e) {
            throw new FrameworkException("Error getting rows from table", e);
        }
    }

    public String findTableRowElement(WebElement element){
        try{
            new WebDriverWait(driver, elementTimeout).until(
                    ExpectedConditions.visibilityOf(element));
            return element.getText();
        } catch (Exception e) {
            throw new FrameworkException("Error getting rows from table", e);
        }
    }

    public void verifyText(WebElement element, String id){
        try{
            if(!id.equals(getElementText(element))){
                throw new FrameworkException("Text Verification failed for given element and value");
            }
        } catch (Exception e) {
            throw new FrameworkException("Exception during verification of text", e);
        }
    }
}
