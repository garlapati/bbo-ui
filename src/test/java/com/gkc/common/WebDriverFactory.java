package com.gkc.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;

public class WebDriverFactory {

    public static enum Browser{
        CHROME, IE, FIREFOX;
    }

    public static WebDriver driver;

    public static WebDriver initializeDriver(Browser browser){
        File file;
        DesiredCapabilities browserCapabiliities;
        try{
            switch (browser){
                case CHROME:
                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "none");
                    file = new File(System.getProperty("user.dir")+"/src/test/resources/drivers/chromedriver.exe");
                    System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
                    driver = new ChromeDriver();
                    driver.manage().window().maximize();
                    break;

                case IE:
                    browserCapabiliities = DesiredCapabilities.internetExplorer();
                    browserCapabiliities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

                    InternetExplorerOptions ieOptions = new InternetExplorerOptions();
                    ieOptions.merge(browserCapabiliities);

                    file = new File(System.getProperty("user.dir")+"/src/test/resources/drivers/IEDriverServer.exe");
                    System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
                    driver = new InternetExplorerDriver(ieOptions);
                    driver.manage().window().maximize();
                    break;

                case FIREFOX:
                    break;
            }
        } catch (Exception exception) {
            //To do Exception
        }
        return driver;
    }
}
