package com.gkc.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ARTProperties {

    private static String CONFIG = "\\src\\test\\resources\\config.properties";

    private static Properties prop = new Properties();

    public static File getFilePath(String filename){
        return new File(System.getProperty("user.dir")+File.separator+ filename);
    }

    public static Properties loadProperties(){
        if(prop.isEmpty()){
            try (InputStream input = new FileInputStream(getFilePath(CONFIG))){
                prop.load(input);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return prop;
    }
}
