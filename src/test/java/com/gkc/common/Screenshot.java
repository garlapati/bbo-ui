package com.gkc.common;

import com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;

import java.io.IOException;

public class Screenshot extends DriverActions{
    public static void getBase64Screenshot() {
        try{
            ExtentCucumberAdapter.addTestStepScreenCaptureFromPath("data:image/png;base64,"+ ((TakesScreenshot)DriverActions.getInstance().getDriver()).getScreenshotAs(OutputType.BASE64));
        } catch (WebDriverException | IOException e) {
            throw new FrameworkException(e);
        }
    }
}
