package com.gkc.main;


import com.gkc.common.DriverActions;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"com.gkc.stepdefinition","com.gkc.common"},
        plugin = {
                "pretty",
                "com.gkc.email.EmailConcurrentEventListener",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"
        },
        monochrome = true
)
public class TestRunner {

    @AfterClass
    public static void quitBrowser(){
        DriverActions.getInstance().quitBrowser();
    }
}
