package com.gkc.pages.elements;

import com.gkc.common.DriverActions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPO {

    protected LoginPO() {
        PageFactory.initElements(DriverActions.getInstance().getDriver(), this);
    }

    //Login

    @FindBy(name = "organisationId") public WebElement txtOrgID;
    @FindBy(name = "userID") public WebElement txtUserID;
    @FindBy(name = "password") public WebElement txtPassword;
    @FindBy(xpath = "//button[text()='Login']") public WebElement btnLogin;




}
