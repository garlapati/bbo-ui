package com.gkc.pages.methods;

import com.gkc.common.ARTProperties;
import com.gkc.common.DriverActions;
import com.gkc.common.FrameworkException;
import com.gkc.pages.elements.LoginPO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

public class LoginActions extends LoginPO {

    private static LoginActions loginActions;
    private final DriverActions driverAction;
    private static final Logger logger = LogManager.getLogger(LoginActions.class.getName());
    static Properties property = ARTProperties.loadProperties();


    public LoginActions(){
        driverAction = DriverActions.getInstance();
    }

    public static LoginActions getInstance(){
        if(loginActions ==null){
            loginActions = new LoginActions();
        }
        return loginActions;
    }

    public void navigateToURL(String env){
        logger.info("Invoke Application");
        driverAction.navigateTo(property.getProperty(env));
        driverAction.waitUntilPageReadyStateComplete();
    }

    public void loginToApplication(String orgID, String username, String password){
        try{
            driverAction.enterText(txtOrgID, orgID);
            driverAction.enterText(txtUserID, username);
            driverAction.enterText(txtPassword, password);
            driverAction.clickButton(btnLogin);
        } catch (Exception exception) {
            logger.error("Failed to login to application"+ exception.getMessage());
            throw new FrameworkException("Failed to login to application", exception);
        }
    }

}
